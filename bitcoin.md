---
layout: default
permalink: /bitcoin-only/
---

# Bitcoin Only Projects

## Current

### Bitcoin Related Docker Containers

These are all self building docker containers for use with single board computing (such as the raspberry pi), some other exotic setup (like the pinephone) or servers
.
* [docker-specter-desktop](https://github.com/lncm/docker-specter-desktop) - Am a big fan of this project, so have decided to create a version which works with docker for server setups and single board computing setups such as the Raspberry PI.
* [docker-lnd-unlock](https://github.com/lncm/docker-lnd-unlock) - As LND does not unlock, and some setups may require this I've created a container which handles this. I plan to also add wallet generation onto this too so the full cycle can be automated. Used for single board computing setups or servers.
* [docker-lnd-neutrino-switch](https://github.com/lncm/docker-lnd-neutrino-switch) - A helper container which switches between neutrino mode to bitcoin mode. Neutrino mode so that LND is functional.
* [docker-clightning](https://gitlab.com/nolim1t/docker-clightning) - CLightning with the RPC plugin in a self building to github and gitlab (for maximum sovereignity) docker container
* [docker-lnd-pool](https://gitlab.com/nolim1t/docker-lnd-pool) - LND Pool reproducable build docker container.

### Other related containers

* [docker-tor](https://github.com/lncm/docker-tor) - I've created a docker container which self builds tor and deploys this to dockerhub. Eventually I want to have this on gitlab too.


## Past / Backlog

Stuff which is still interesting but more either backlogged or just have contributor status.

### The Box Project

The idea behind this project was to make full nodes as easy as possible, utilizing docker for more modular development.

I plan to make this more open and inclusive and support pruned nodes (and low powered devices too).

Currently on the sidelines for now, while taking a look at some non-custodial technologies for helping people stack sats while they sleep - think joinmarket or lightning pool.

#### Links

* [pi-factory](https://github.com/lncm/pi-factory)
* [thebox-compose-system](https://github.com/lncm/thebox-compose-system)

### Umbrel Project

I've been a [big contributor](https://github.com/getumbrel/umbrel/graphs/contributors) for a lot of the early underlying features for the Umbrel project as a lot of it was reused in previous stuff I've been working on.

Am still interested in monitoring this project as they do have a lot of nice UX which is helpful at onboarding people to it.

They also provide good community support.

