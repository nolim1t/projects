---
layout: default
permalink: /projects/
---

# Projects

## Current

### Blogging Project

I actively maintain two blogs and sites.

#### Technology Blog

**Link:** [nolim1t.co](https://nolim1t.co)

This site that you're on being a technology focused site, and my window into my world of technology where it interests me.

I generally write about the following:

* Web Technologies
* Mobile Technologies
* Blockchain Technologies
* Cryptocurrency trading
* Changes with this site

On the technology blog I often add some technology previews too.

##### Technologies / Solutions used


This site runs off a jekyll instance (currently hosted on gitlab to learn how to use it), but I've also cleverly split it up into multiple repositiories to keep things modularized. Also runs off github for backwards compatibility because jekyll sites should technically work both ways.

I have a layouts folder (for changing the whole look and feel), projects folder (this page!), opensource folder (list of Open Source I've done), and the main blogging page.

The site uses mostly a custom template, but I've incorporated a few react widgets into it to familiarize myself with react. And lately I've spliced in some Vue.js code too.

#### Travel / Food Blog

**Link:** [itinerantfoodie.com](https://itinerantfoodie.com)

This is my other passion! Eat well, travel well, and take lots of amazing photos.

I write about -

* Travel featured destinations
* Travel tips
* Notable Restaurants in the world
* Notable food in the world
* Products I endorse

I also use this site to feature some of the most recent photos I've taken too.

##### Technologies / Solutions used

Almost the same as my technologies as my tech blog, except for one main difference that I'm hosting the site on Amazon S3 (and also cloudfront). In a summary:

* AWS S3
* AWS CloudFront
* Lets Encrypt (also using my [docker tool](https://github.com/nolim1t/docker-letsencrypt) to mass renew all my certificates)
* AWS Lambda for image resizing
* Travis CI for deployment automation

I have also build my own uploader tool behind the scenes for uploading AND automatically resizing images (through Amazon Lambda) for the website alone. This allows me to take photos, and upload them directly from my phone rather than having to copy them to a laptop, edit the images, and then upload the images.

Another cool feature I have is that I'm still using github to write posts because it is a nice tool to do version control, however the difference is that I have a travis CI configuration which automatically updates the Amazon S3 bucket.

I've basically built something thats as easy to update as possible, but without using wordpress (which is slow, and is a pain to copy). I just want to blog, I don't care about databases.

I've also delved in react a bit more further and went ahead and created a recent instagrams widget, which shows the last 6 instagrams as I love to share to more channels.

### Umbrel project

This is a spinoff of the [LNCM.io](https://lncm.io/project/box/) box project. Some improvements to general UX/Usability and some improved middleware. For more details on the project or contributing to it check out [GetUmbrel](https://getumbrel.com) or [Umbrel Github](https://github.com/getumbrel/)


#### Active Technologies

* Docker (for orchestration and modularization) as well as a [web installer](https://github.com/getumbrel/umbrel-compose) for getting the box online
* [Raspbian Raspberry PI orchestration](https://github.com/getumbrel/umbrel-os)
* Node.js
* bash Scripting
* VUEJS for frontend

#### Proposed technologies / works

* Slick Wallet Management UI/UX
* API
* Full open Source

### "The Box" project

What started off in a random hackathon with some friends with enthusiasts almost 6 months ago has now ballooned into a fully functional product, with working in the point of sale (live in an actual business).

The box is a retail box which is completely open source, and hardware can be sourced with commodity hardware. The aim is to be able to build something with as cheap as possible materials (but also functional) and to target developing countries or countries with high import tax on electronics.

Another aim is to provide merchants with an idea of self-sufficiency where they have full control their own assets, rather than being a financial custodian which requires being a regulated entity.

The box is also extendable (as long as the hardware supports it) to support other technologies, and has a set of APIs which can be built on top of it.

For more information check out [LNCM.io](https://lncm.io/project/box/)

#### Active Technologies

* Docker (for orchestration and modularization) as well as a [custom python library](https://github.com/lncm/noma)
* [Alpine Linux Raspberry PI orchestration](https://github.com/lncm/pi-factory)
* Go
* ReactJS for frontend

#### Proposed technologies / works

* e-Commerce integration
* Integration with another POS
* Mobile Application support

## Completed

## Client projects

### LINE chat bot application

For a short project, I was tasked to create a LINE messenger chatbot which responded to different commands with either text based responses or more rich base responses.

The concept is very similar to IRC chat bots which I've worked on "back in the day", except the receiving interface as all through a HTTP/s endpoint which you process.

#### Technologies used

* Node.js
* AWS Lambda

### HeyAgents.com.au backend

Beginning of 2017, I've been tasked to create a backend API interface for HeyAgents.com.au. This was purely done on node.js and purely runs off AWS Lambda.

This project had a bit of challenges being AWS Lambda is a very new technology. I've created a few open source libraries along the way.

#### Open Source Libraries

* [**decode-post-body-params**](https://github.com/nolim1t/decode-post-body-params) - Converting the body received by AWS Lambda to an actual variable. Would love to change this so that it converts it to a javascript object.
* [**fuckingundefinedemptynull**](https://github.com/nolim1t/fuckingundefinedemptynull) - Detecting or undefines nulls the right way. Basically, a general javascript issue - but it was causing a few bugs

#### Technologies used

* Node.js
* AWS Lambda
* AWS S3
* AWS DynamoDB

## Fintech Projects

### Trading Application for BitMex / Poloniex

Currently, it's a manually laborious task for logging into [bitmex](https://www.bitmex.com/register/TjGKqo) and / poloniex just to execute trades.

Eventually, I'd like to be able to execute trades (which automatically set a conservative stop loss and take profit orders) through the command line. As Poloniex has no stop loss feature which closes positions, I'd like to incorporate this as part of the application (monitors the position and closes it out if it gets below a certain threshold)

#### Technologies / Solutions used

* Node.js
* AWS Lambda and AWS Cloudwatch for poloniex stop loss monitoring feature

#### Open Source Libraries used

* [**nl-poloniex**](https://github.com/nolim1t/nl-poloniex)
* [**nl-bitmex**](https://github.com/nolim1t/nl-bitmex)

### Lightning ⚡️ Network Invoice Generator

Created a template framework for both generating a lightning network invoice (as well as fallback crypto) for blog posts and custom invoices.

Don't know what lightning network is? Try [this link](http://lightning.network/) or [this video](https://www.youtube.com/watch?v=rrr_zPmEiME)

#### Technologies used

* Vue.js (frontend) + Jekyll flavored templates (because fuck wordpress)
* Axios - For HTTP get that works nicely with Vue.
* AWS Lambda - backend for interfacing with the payment network for generating invoices, or checking payments in a secure fashion.
* **[nl-bxth](https://github.com/nolim1t/nl-bxth)** - Exchange library for getting an aggregate rate with hypothetical liquidity.

### Cryptocurrency Invoice Generator

Using the coinpayments.net merchant API, I've built a tool to create an invoice link in cryptocurrency.

The tool can be used <a href="https://bn2snfmfz4.execute-api.us-east-1.amazonaws.com/1">here</a>, however it uses my own merchant id. It is hosted on AWS Lambda, so there is no hosting costs.

#### Open Source Libraries used

* [**generate-coinpayments-link**](https://github.com/nolim1t/generate-coinpayments-link) - Created for this purpose.
